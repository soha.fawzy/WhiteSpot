package com.xdev.whitespot.adapters;


import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xdev.whitespot.R;
import com.xdev.whitespot.views.OrderDetailsActivity;
import com.xdev.whitespot.webservices.ClientOrdersResponse;

import java.util.List;

public class ClientOrdersAdapter extends RecyclerView.Adapter<ClientOrdersAdapter.BH> {
    private Activity mContext;
    private List<ClientOrdersResponse.Orders> orders;

    public ClientOrdersAdapter(Activity mContext, List<ClientOrdersResponse.Orders> orders) {
        this.mContext = mContext;
        this.orders = orders;
    }

    @Override
    public ClientOrdersAdapter.BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View dayView = LayoutInflater.from(mContext).inflate(R.layout.client_order_item, parent, false);
        return new ClientOrdersAdapter.BH(dayView);
    }

    @Override
    public void onBindViewHolder(ClientOrdersAdapter.BH holder, int position) {
        if (position % 2 == 0) {
            holder.card.setBackgroundColor(mContext.getResources().getColor(R.color.pur));
        }
        holder.name.setText(mContext.getString(R.string.order) + Integer.toString(position + 1));
        holder.price.setText(Float.toString(orders.get(position).Total));
        holder.state.setText(orders.get(position).State);
        holder.card.setOnClickListener(view -> mContext.startActivity(new Intent(mContext, OrderDetailsActivity.class).putExtra("order", orders.get(position))));

    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public static class BH extends RecyclerView.ViewHolder {
        TextView name, price, state;
        ConstraintLayout card;


        public BH(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card);
            name = itemView.findViewById(R.id.textView33);
            price = itemView.findViewById(R.id.textView34);
            state = itemView.findViewById(R.id.textView35);

        }
    }
}
