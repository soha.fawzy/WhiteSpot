package com.xdev.whitespot.adapters;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.Utils;
import com.xdev.whitespot.webservices.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.BH> {
    private final SelectedItemsAdapter selectedItemsAdapter;
    TextView total;
    private Activity mContext;
    private List<Category.AllCategories.Item> items;
    private List<Category.AllCategories.Item> selectedItems = new ArrayList<>();

    public CategoryAdapter(Activity mContext, List<Category.AllCategories.Item> items, RecyclerView selected, TextView total) {
        this.mContext = mContext;
        this.items = items;
        this.total = total;
        selectedItemsAdapter = new SelectedItemsAdapter(mContext, selectedItems, total);
        selected.setLayoutManager(new LinearLayoutManager(mContext));
        selected.setAdapter(selectedItemsAdapter);
    }

    @Override
    public CategoryAdapter.BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View dayView = LayoutInflater.from(mContext).inflate(R.layout.item, parent, false);
        return new CategoryAdapter.BH(dayView);
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.BH holder, int position) {
        if (items.get(position).selected) {
            holder.icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_remove_circle_outline_black_24dp));
        } else {
            holder.icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_add_circle_outline_black_24dp));

        }
        final Float[] price = new Float[1];
        if (Utils.isRTL(Locale.getDefault())) {
            holder.name.setText(items.get(position).ArName);

        } else {
            holder.name.setText(items.get(position).EngName);
        }
        holder.price.setText(items.get(position).Price);
        holder.icon.setOnClickListener(view -> {
            if (!items.get(position).selected) {
                selectedItems.add(items.get(position));
                notifyDataSetChanged();
                selectedItemsAdapter.notifyDataSetChanged();
            } else {
                price[0] = selectedItemsAdapter.getCount(items.get(position).EngName) * Float.parseFloat(items.get(position).Price);
                D.price -= price[0];
                selectedItemsAdapter.removeById(items.get(position).EngName);
                notifyDataSetChanged();
                total.setText(mContext.getString(R.string.total_cost) + Float.toString(D.price) + " " + mContext.getString(R.string.currency));
            }
            items.get(position).selected = !items.get(position).selected;


        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class BH extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView name, price;

        public BH(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.button);
            name = itemView.findViewById(R.id.itemName);
            price = itemView.findViewById(R.id.itemPrice);
        }
    }
}

