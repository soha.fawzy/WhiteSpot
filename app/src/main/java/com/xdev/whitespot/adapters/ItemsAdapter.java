package com.xdev.whitespot.adapters;


import android.app.Activity;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xdev.whitespot.R;
import com.xdev.whitespot.webservices.OrderDetailsResponse;

import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.BH> {
    private Activity mContext;
    private List<OrderDetailsResponse.OrderDetails> orderDetails;

    public ItemsAdapter(Activity mContext, List<OrderDetailsResponse.OrderDetails> orderDetails) {
        this.mContext = mContext;
        this.orderDetails = orderDetails;
    }

    @Override
    public ItemsAdapter.BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View dayView = LayoutInflater.from(mContext).inflate(R.layout.history_subitem, parent, false);
        return new ItemsAdapter.BH(dayView);
    }

    @Override
    public void onBindViewHolder(ItemsAdapter.BH holder, int position) {
        if (position % 2 == 0) {
            holder.card.setBackgroundColor(mContext.getResources().getColor(R.color.pur));
        } else {
            holder.card.setBackgroundColor(mContext.getResources().getColor(R.color.purple));

        }
        holder.name.setText(Integer.toString(orderDetails.get(position).Qte) + " " + orderDetails.get(position).ItemEngName);
        holder.price.setText(Float.toString(orderDetails.get(position).Price * orderDetails.get(position).Qte) + mContext.getString(R.string.currency));

    }

    @Override
    public int getItemCount() {
        return orderDetails.size();
    }

    public static class BH extends RecyclerView.ViewHolder {
        TextView name, price;
        ConstraintLayout card;


        public BH(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card);
            name = itemView.findViewById(R.id.textView31);
            price = itemView.findViewById(R.id.textView32);

        }
    }
}


