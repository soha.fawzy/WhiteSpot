package com.xdev.whitespot.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.Utils;
import com.xdev.whitespot.webservices.AddOrderRequest;
import com.xdev.whitespot.webservices.Category;

import java.util.List;
import java.util.Locale;

public class SelectedItemsAdapter extends RecyclerView.Adapter<SelectedItemsAdapter.BH> {
    TextView total;
    AddOrderRequest.OrderedItem order;
    private Activity mContext;
    private List<Category.AllCategories.Item> items;
    private int count = 1;

    public SelectedItemsAdapter(Activity mContext, List<Category.AllCategories.Item> items, TextView total) {
        this.mContext = mContext;
        this.items = items;
        this.total = total;
    }

    @Override
    public SelectedItemsAdapter.BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View dayView = LayoutInflater.from(mContext).inflate(R.layout.selected_item, parent, false);
        return new SelectedItemsAdapter.BH(dayView);
    }

    @Override
    public void onBindViewHolder(SelectedItemsAdapter.BH holder, int position) {
        if (Utils.isRTL(Locale.getDefault())) {
            holder.name.setText(items.get(position).ArName);

        } else {
            holder.name.setText(items.get(position).EngName);
        }
        holder.count.setText("1");
        items.get(position).count = 1;
        order = new AddOrderRequest.OrderedItem(items.get(position).Id, items.get(position).count);
        D.orderedItems.add(order);
        D.price += Float.parseFloat(items.get(position).Price);
        total.setText(mContext.getString(R.string.total_cost) + Float.toString(D.price) + " " + mContext.getString(R.string.currency));
        holder.add.setOnClickListener(view -> {
            D.orderedItems.remove(order);
            count++;
            items.get(position).count = count;
            order = new AddOrderRequest.OrderedItem(items.get(position).Id, items.get(position).count);
            D.orderedItems.add(order);
            D.price += Float.parseFloat(items.get(position).Price);
            holder.count.setText(Integer.toString(count));
            total.setText("Total Cost " + Float.toString(D.price) + " " + mContext.getString(R.string.currency));

        });
        holder.remove.setOnClickListener(view -> {
            D.price -= Float.parseFloat(items.get(position).Price);
            total.setText(mContext.getString(R.string.total_cost) + Float.toString(D.price) + " " + mContext.getString(R.string.currency));
            D.orderedItems.remove(position);
            if (count > 1) {
                count--;
                items.get(position).count = count;
                D.orderedItems.remove(order);
                order = new AddOrderRequest.OrderedItem(items.get(position).Id, items.get(position).count);
                D.orderedItems.add(order);
                holder.count.setText(Integer.toString(count));
            } else {
                remove(position);
            }

        });


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void remove(int position) {
        items.remove(position);
        mContext.runOnUiThread(this::notifyDataSetChanged);
    }

    public void removeById(String name) {
        for (Category.AllCategories.Item item : items) {
            if (item.EngName.equals(name) || item.ArName.equals(name)) {
                remove(items.indexOf(item));
                for (AddOrderRequest.OrderedItem orderedItem : D.orderedItems) {
                    if (orderedItem.ItemId == item.Id) {
                        D.orderedItems.remove(D.orderedItems.indexOf(orderedItem));
                    }
                }
                return;
            }
        }
    }

    public int getCount(String name) {
        int c = 0;
        for (Category.AllCategories.Item item : items) {
            if (item.EngName.equals(name) || item.ArName.equals(name)) {
                c = item.count;
            }
        }
        return c;
    }

    public static class BH extends RecyclerView.ViewHolder {
        ImageView add, remove;
        TextView name, count;

        public BH(View itemView) {
            super(itemView);
            add = itemView.findViewById(R.id.imageView12);
            remove = itemView.findViewById(R.id.imageView11);
            name = itemView.findViewById(R.id.textView16);
            count = itemView.findViewById(R.id.textView23);
        }
    }
}

