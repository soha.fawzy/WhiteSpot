package com.xdev.whitespot.adapters;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xdev.whitespot.R;
import com.xdev.whitespot.webservices.HistoryResponse;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.BH> {
    private Activity mContext;
    private List<HistoryResponse.OrderDetails> orderDetails;

    public HistoryAdapter(Activity mContext, List<HistoryResponse.OrderDetails> orderDetails) {
        this.mContext = mContext;
        this.orderDetails = orderDetails;
    }

    @Override
    public HistoryAdapter.BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View dayView = LayoutInflater.from(mContext).inflate(R.layout.history_item, parent, false);
        return new HistoryAdapter.BH(dayView);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.BH holder, int position) {
        holder.name.setText(mContext.getString(R.string.order) + Integer.toString(position + 1));
        holder.price.setText(orderDetails.get(position).Total);
        holder.pickupTime.setText(mContext.getString(R.string.pickuptime) + orderDetails.get(position).PickupTime);
        holder.deliveryTime.setText(mContext.getString(R.string.deliverytime) + orderDetails.get(position).DeliveryTime);
        if (orderDetails.get(position).EngState.equals(mContext.getString(R.string.waiting))) {
            holder.pickupIcon.setVisibility(View.GONE);
            holder.deliveryIcon.setVisibility(View.GONE);
        } else if (orderDetails.get(position).EngState.equals(mContext.getString(R.string.delivered))) {
            holder.pickupIcon.setVisibility(View.VISIBLE);
            holder.deliveryIcon.setVisibility(View.VISIBLE);
        } else {
            holder.pickupIcon.setVisibility(View.VISIBLE);
            holder.deliveryIcon.setVisibility(View.GONE);
        }
        if (orderDetails.get(position).items != null) {
            ItemsAdapter itemsAdapter = new ItemsAdapter(mContext, orderDetails.get(position).items);
            holder.items.setLayoutManager(new LinearLayoutManager(mContext));
            holder.items.setAdapter(itemsAdapter);
        }


    }

    @Override
    public int getItemCount() {
        return orderDetails.size();
    }

    public static class BH extends RecyclerView.ViewHolder {
        ImageView pickupIcon, deliveryIcon;
        TextView name, price, pickupTime, deliveryTime;
        RecyclerView items;


        public BH(View itemView) {
            super(itemView);
            pickupIcon = itemView.findViewById(R.id.imageView20);
            deliveryIcon = itemView.findViewById(R.id.imageView21);
            name = itemView.findViewById(R.id.textView28);
            price = itemView.findViewById(R.id.textView27);
            pickupTime = itemView.findViewById(R.id.textView29);
            deliveryTime = itemView.findViewById(R.id.textView30);
            items = itemView.findViewById(R.id.recyclerView8);
        }
    }
}

