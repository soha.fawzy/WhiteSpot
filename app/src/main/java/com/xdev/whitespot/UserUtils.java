package com.xdev.whitespot;

import android.content.Context;


public class UserUtils {

    public static final String USER_FILE = "user";
    public static final String ACCOUNT_STATUS = "accountStatus";
    private static final transient String TAG = User.class.getSimpleName();

    public static void saveUser(Context context) {
        Utils.writeObject(context, D.sSelf, USER_FILE);
        Utils.writeObject(context, D.sAccountStatus, ACCOUNT_STATUS);

    }

    public static void clearUser(Context context) {
        D.sSelf = new User();
        D.sAccountStatus = D.AccountStatus.LOGGED_OUT;
        saveUser(context);
    }
}
