package com.xdev.whitespot.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.adapters.CategoryAdapter;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.Category;
import com.xdev.whitespot.webservices.Order;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Step2Activity extends AppCompatActivity {
    TextView clean, press, total;
    RecyclerView men, women, general, common, SelectedItems, SelectedItems2, SelectedItems3, SelectedItems4;
    Order order;
    CategoryAdapter menCategory, womenCategory, generalCategory, commonCategory;
    ImageView Men, Women, General, Common;
    String need = "press";
    int mainCat;
    Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step2);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.order_details));
        toolbar.setNavigationOnClickListener(view -> finish());
        FloatingActionButton floatingActionButton = findViewById(R.id.float_action);
        floatingActionButton.setOnClickListener(view -> {
            String uri = "tel: 9999";
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        });
        press = findViewById(R.id.textView10);
        clean = findViewById(R.id.textView11);
        men = findViewById(R.id.recyclerView);
        women = findViewById(R.id.recyclerView2);
        general = findViewById(R.id.recyclerView3);
        common = findViewById(R.id.recyclerView4);
        Men = findViewById(R.id.menbtn);
        Women = findViewById(R.id.womenbtn);
        General = findViewById(R.id.generalbtn);
        Common = findViewById(R.id.commonbtn);
        SelectedItems = findViewById(R.id.recyclerView5);
        SelectedItems2 = findViewById(R.id.recyclerView6);
        SelectedItems3 = findViewById(R.id.recyclerView7);
        SelectedItems4 = findViewById(R.id.recyclerView8);
        total = findViewById(R.id.textView15);
        next = findViewById(R.id.button6);
        if (getIntent().hasExtra("order")) {
            order = (Order) getIntent().getSerializableExtra("order");
        }
        switch (order.TypeId) {
            case 1:
                mainCat = 1;
                break;
            case 2:
                mainCat = 2;
                break;
            case 3:
                mainCat = 3;
                break;
        }

        getItems();

        clean.setOnClickListener(view -> {
            clean.setTextColor(this.getResources().getColor(R.color.yellow));
            press.setTextColor(this.getResources().getColor(R.color.purple));
            need = "clean";
            switch (order.TypeId) {
                case 1:
                    mainCat = 2;
                    break;
                case 2:
                    mainCat = 4;
                    break;
                case 3:
                    mainCat = 6;
                    break;
            }
            getItems();
            D.price = 0;
            total.setText(getString(R.string.total_cost) + Float.toString(D.price) + getString(R.string.currency));

        });

        press.setOnClickListener(view -> {
            clean.setTextColor(this.getResources().getColor(R.color.purple));
            press.setTextColor(this.getResources().getColor(R.color.yellow));
            need = "press";
            switch (order.TypeId) {
                case 1:
                    mainCat = 1;
                    break;
                case 2:
                    mainCat = 2;
                    break;
                case 3:
                    mainCat = 3;
                    break;
            }
            getItems();
            D.price = 0;
            total.setText(getString(R.string.total_cost) + Float.toString(D.price) + getString(R.string.currency));

        });

        Men.setOnClickListener(view -> men.setVisibility(View.VISIBLE));
        Women.setOnClickListener(view -> women.setVisibility(View.VISIBLE));
        General.setOnClickListener(view -> general.setVisibility(View.VISIBLE));
        Common.setOnClickListener(view -> common.setVisibility(View.VISIBLE));

        next.setOnClickListener(view -> {
            if (D.orderedItems.size() == 0) {
                Toast.makeText(this, "Please select items", Toast.LENGTH_SHORT).show();
                return;
            }

            startActivity(new Intent(Step2Activity.this, Step3Activity.class).
                    putExtra("order", order).putExtra("need", total.getText().toString().substring(11)).
                    addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));

        });
    }

    public void getItems() {
        App.getService().getCategories(mainCat).enqueue(new Callback<Category>() {
            @Override
            public void onResponse(@NonNull Call<Category> call, @NonNull Response<Category> r) {
                if (r.body() == null || r.body().status != 200) {
                    onFailure(call, new Throwable());
                    return;
                }

                for (Category.AllCategories category : r.body().ItemsOfAllCategories) {
                    switch (category.CategoryEngName) {
                        case "Men":
                            menCategory = new CategoryAdapter(Step2Activity.this, category.Items, SelectedItems, total);
                            men.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            men.setAdapter(menCategory);
                            break;
                        case "Women":
                            womenCategory = new CategoryAdapter(Step2Activity.this, category.Items, SelectedItems2, total);
                            women.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            women.setAdapter(womenCategory);
                            break;
                        case "General":
                            generalCategory = new CategoryAdapter(Step2Activity.this, category.Items, SelectedItems3, total);
                            general.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            general.setAdapter(generalCategory);
                            break;
                        case "Common":
                            commonCategory = new CategoryAdapter(Step2Activity.this, category.Items, SelectedItems4, total);
                            common.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            common.setAdapter(commonCategory);
                            break;
                    }
                }


            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
