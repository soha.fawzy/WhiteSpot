package com.xdev.whitespot.views;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.UserUtils;
import com.xdev.whitespot.Utils;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.EditProfileRequest;
import com.xdev.whitespot.webservices.LoginResponse;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFragment extends Fragment {
    EditText email, username, password, address, phone;
    Button edit;
    private ProgressDialog mDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        email = rootView.findViewById(R.id.editText);
        username = rootView.findViewById(R.id.editText2);
        password = rootView.findViewById(R.id.editText3);
        address = rootView.findViewById(R.id.editText6);
        email.setText(D.sSelf.Email);
        username.setText(D.sSelf.Name);
        address.setText(D.sSelf.Address);
        phone = rootView.findViewById(R.id.editText9);
        phone.setText(D.sSelf.Number);

        edit = rootView.findViewById(R.id.button3);
        edit.setOnClickListener(view -> {
            showProgressDialog();
            if (!password.getText().toString().equals("")) {
                App.getService().edit(new EditProfileRequest(D.sSelf.Id, password.getText().toString(), email.getText().toString(), phone.getText().toString(), address.getText().toString())).enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.body() == null || response.body().Status != 200) {
                            onFailure(call, new Throwable());
                            return;
                        }
                        D.sSelf = response.body().EditedUser;
                        UserUtils.saveUser(getContext());
                        password.setText("");
                        dismissProgressDialog();


                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        t.printStackTrace();
                        email.setText(D.sSelf.Email);
                        username.setText(D.sSelf.Name);
                        address.setText(D.sSelf.Address);
                        password.setText("");
                        phone.setText(D.sSelf.Number);
                        dismissProgressDialog();

                    }
                });

            } else {
                App.getService().edit(new EditProfileRequest(D.sSelf.Id, email.getText().toString(), phone.getText().toString(), address.getText().toString())).enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.body() == null || response.body().Status != 200) {
                            onFailure(call, new Throwable());
                            return;
                        }
                        D.sSelf.Email = email.getText().toString();
                        D.sSelf.Number = phone.getText().toString();
                        D.sSelf.Address = address.getText().toString();
                        UserUtils.saveUser(getContext());
                        dismissProgressDialog();


                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        t.printStackTrace();
                        if (t.fillInStackTrace() instanceof UnknownHostException || t.fillInStackTrace() instanceof SocketTimeoutException) {
                            {
                                Utils.showSnackBar(getActivity(), R.string.no_internet);
                            }
                        }
                        email.setText(D.sSelf.Email);
                        username.setText(D.sSelf.Name);
                        address.setText(D.sSelf.Address);
                        password.setText("");
                        phone.setText(D.sSelf.Number);
                        dismissProgressDialog();

                    }
                });
            }
        });
        return rootView;
    }

    private void showProgressDialog() {
        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Updating");
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    private void dismissProgressDialog() {
        if (null != mDialog) {
            mDialog.dismiss();
        }
    }

}
