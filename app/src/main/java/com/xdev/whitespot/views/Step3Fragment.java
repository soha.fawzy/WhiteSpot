package com.xdev.whitespot.views;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.webservices.AddOrderRequest;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.Order;
import com.xdev.whitespot.webservices.StatusResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Step3Fragment extends Fragment {

    ImageView hanging, folded, strach;
    EditText comment;
    String need, packageMethod = "Hanging";
    Button submit;
    Order order;
    private ProgressDialog mDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_step3, container, false);
        hanging = rootView.findViewById(R.id.imageView17);
        folded = rootView.findViewById(R.id.imageView18);
        strach = rootView.findViewById(R.id.imageView19);
        comment = rootView.findViewById(R.id.editText7);
        submit = rootView.findViewById(R.id.button7);
        need = getArguments().getString("need");
        order = (Order) getArguments().getSerializable("order");
        hanging.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
        folded.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
        strach.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));

        hanging.setOnClickListener(view -> {
            hanging.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
            folded.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
            strach.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
            packageMethod = "Hanging";

        });

        folded.setOnClickListener(view -> {
            hanging.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
            folded.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
            strach.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
            packageMethod = "Folded";

        });

        strach.setOnClickListener(view -> {
            hanging.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
            folded.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
            strach.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
            packageMethod = "Strach";
        });

        submit.setOnClickListener(view -> {
            showProgressDialog();
            List<AddOrderRequest.OrderedItem> orderedItems = new ArrayList<>();
            orderedItems.addAll(D.orderedItems);
            App.getService().addOrder(new AddOrderRequest(new AddOrderRequest.UserData(D.sSelf.Id, D.sSelf.Name, order.Address, order.deliveryWay, packageMethod, order.pickupTime, order.deliverTime, D.sSelf.Number, D.price, order.Address, order.Address, comment.getText().toString()), orderedItems)).enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> r) {
                    if (r.body() == null) {
                        onFailure(call, new Throwable());
                        return;
                    }
                    dismissProgressDialog();
                    Fragment f = new AddOrderFragment();
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.action_container, f);
                    ft.commit();
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Home");
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    t.printStackTrace();
                    dismissProgressDialog();

                }
            });
        });
        return rootView;
    }

    private void showProgressDialog() {
        mDialog = new ProgressDialog(getContext());
        mDialog.setMessage("Submitting");
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    private void dismissProgressDialog() {
        if (null != mDialog) {
            mDialog.dismiss();
        }
    }
}
