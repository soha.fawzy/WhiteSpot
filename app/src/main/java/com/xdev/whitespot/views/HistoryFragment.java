package com.xdev.whitespot.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.adapters.HistoryAdapter;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.HistoryResponse;
import com.xdev.whitespot.webservices.OrderDetailsResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment {
    List<HistoryResponse.OrderDetails> historyList = new ArrayList<>();
    HistoryAdapter historyAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.history);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        historyAdapter = new HistoryAdapter(getActivity(), historyList);
        recyclerView.setAdapter(historyAdapter);
        getHistory();
        return rootView;
    }

    public void getHistory() {
        App.getService().getHistory(D.sSelf.Id).enqueue(new Callback<HistoryResponse>() {
            @Override
            public void onResponse(Call<HistoryResponse> call, Response<HistoryResponse> r) {
                if (r.body() == null) {
                    onFailure(call, new Throwable());
                    return;
                }
                if (r.body().History != null || r.body().History.size() != 0) {
                    for (int i = r.body().History.size() - 1; i >= 0; i--) {

                        int finalI = i;
                        HistoryResponse.OrderDetails orderDetails = r.body().History.get(finalI);
                        App.getService().getOrderDetails(r.body().History.get(i).Id).enqueue(new Callback<OrderDetailsResponse>() {
                            @Override
                            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                                if (response.body() == null) {
                                    onFailure(call, new Throwable());
                                    return;
                                }
                                orderDetails.items = new ArrayList<>();
                                orderDetails.items.addAll(response.body().OrderDetails);
                                orderDetails.CollectingMethod = response.body().OrderData.CollectingMethod;
                                orderDetails.DeliveryTime = response.body().OrderData.DeliveryTime;
                                orderDetails.PickupTime = response.body().OrderData.PickupTime;
                                historyList.add(orderDetails);
                                historyAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                                t.printStackTrace();
                                historyList.add(orderDetails);
                                historyAdapter.notifyDataSetChanged();

                            }
                        });
                    }

                }
            }

            @Override
            public void onFailure(Call<HistoryResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
