package com.xdev.whitespot.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.xdev.whitespot.D;
import com.xdev.whitespot.ForgetPasswordActivity;
import com.xdev.whitespot.Main2Activity;
import com.xdev.whitespot.R;
import com.xdev.whitespot.UserUtils;
import com.xdev.whitespot.Utils;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.LoginRequest;
import com.xdev.whitespot.webservices.LoginResponse;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    EditText username, password;
    CheckBox remember;
    Button SignIn;
    TextView Register, forget;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        username = findViewById(R.id.editText);
        password = findViewById(R.id.editText3);
        remember = findViewById(R.id.checkBox);
        SignIn = findViewById(R.id.button3);
        forget = findViewById(R.id.textView18);
        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActivity.this, ForgetPasswordActivity.class));
            }
        });
        SignIn.setOnClickListener(view -> {
            if (!Utils.isEditTextsEmpty(getApplication(), username, password)) {
                showProgressDialog();
                App.getService().login(new LoginRequest(username.getText().toString(), username.getText().toString(), password.getText().toString())).enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> r) {
                        if (r.body() == null || r.code() != 200) {
                            onFailure(call, new Throwable());
                            return;
                        }
                        switch (r.body().Status) {
                            case 404:
                                username.setError(getString(R.string.not_valid));
                                password.setError(getString(R.string.not_valid));
                                break;
                            case 200:
                                D.sSelf = r.body().ProfileDate;
                                D.sAccountStatus = D.AccountStatus.LOGGED_IN;
                                if (remember.isChecked()) {
                                    UserUtils.saveUser(SignInActivity.this);
                                }
                                if (r.body().ProfileDate.UserTypeText.equals("collector")) {
                                    startActivity(new Intent(SignInActivity.this, Main2Activity.class));

                                } else {
                                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                }
                                break;
                        }
                        dismissProgressDialog();

                    }

                    @Override
                    public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                        dismissProgressDialog();
                        t.printStackTrace();
                        if (t.fillInStackTrace() instanceof UnknownHostException || t.fillInStackTrace() instanceof SocketTimeoutException) {
                            Utils.showSnackBar(SignInActivity.this, R.string.no_internet);
                        }
                    }
                });
            }
        });
        Register = findViewById(R.id.textView6);
        Register.setOnClickListener(view -> startActivity(new Intent(SignInActivity.this, SignupActivity.class)));
    }

    private void showProgressDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.signing_in));
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    private void dismissProgressDialog() {
        if (null != mDialog) {
            mDialog.dismiss();
        }
    }
}
