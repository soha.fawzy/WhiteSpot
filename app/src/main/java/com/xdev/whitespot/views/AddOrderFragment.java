package com.xdev.whitespot.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.xdev.whitespot.R;

public class AddOrderFragment extends Fragment {

    Button addOrder;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_order, container, false);
        addOrder = rootView.findViewById(R.id.button4);
        addOrder.setOnClickListener(view -> onOrderClick());


        return rootView;
    }

    private void onOrderClick() {
        getActivity().startActivity(new Intent(getActivity(), Step1Activity.class));
    }

}
