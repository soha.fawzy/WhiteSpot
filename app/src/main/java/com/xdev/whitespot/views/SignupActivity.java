package com.xdev.whitespot.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.UserUtils;
import com.xdev.whitespot.Utils;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.RegisterRequest;
import com.xdev.whitespot.webservices.RegisterResponse;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    EditText username, phone, email, password, address;
    CheckBox accept;
    Button register;
    TextView login;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        email = findViewById(R.id.editText);
        username = findViewById(R.id.editText2);
        password = findViewById(R.id.editText3);
        phone = findViewById(R.id.editText4);
        address = findViewById(R.id.editText8);
        accept = findViewById(R.id.checkBox2);
        login = findViewById(R.id.textView6);
        login.setOnClickListener(view -> startActivity(new Intent(SignupActivity.this, SignInActivity.class)));
        register = findViewById(R.id.button3);
        register.setOnClickListener(view -> {
            if (!Utils.isEditTextsEmpty(getApplicationContext(), email, username, password, phone, address) || Utils.isValidEmail(getApplicationContext(), email) || Utils.isValidPassword(getApplicationContext(), password) || Utils.isChecked(getApplicationContext(), accept)) {
                showProgressDialog();
                App.getService().register(new RegisterRequest(username.getText().toString(), password.getText().toString(), email.getText().toString(), phone.getText().toString(), address.getText().toString())).enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<RegisterResponse> call, @NonNull Response<RegisterResponse> r) {
                        if (r.body() == null) {
                            onFailure(call, new Throwable());
                            return;
                        }
                        switch (r.body().Status) {
                            case 500:
                                username.setError("username is registered before");
                                break;
                            case 200:
                                D.sSelf = r.body().User;
                                D.sSelf.Number = phone.getText().toString();
                                D.sSelf.Address = address.getText().toString();
                                D.sSelf.UserTypeText = "user";
                                D.sAccountStatus = D.AccountStatus.LOGGED_IN;
                                UserUtils.saveUser(getApplicationContext());
                                startActivity(new Intent(SignupActivity.this, MainActivity.class));
                                break;
                        }
                        dismissProgressDialog();
                    }

                    @Override
                    public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t) {
                        dismissProgressDialog();
                        t.printStackTrace();
                        if (t.fillInStackTrace() instanceof SocketTimeoutException || t.fillInStackTrace() instanceof UnknownHostException) {
                            Utils.showSnackBar(SignupActivity.this, R.string.no_internet);
                        }
                    }
                });

            }
        });
    }


    private void showProgressDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.signing_in));
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    private void dismissProgressDialog() {
        if (null != mDialog) {
            mDialog.dismiss();
        }
    }
}
