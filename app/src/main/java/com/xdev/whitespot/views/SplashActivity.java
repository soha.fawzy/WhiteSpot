package com.xdev.whitespot.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.xdev.whitespot.D;
import com.xdev.whitespot.Main2Activity;
import com.xdev.whitespot.R;

public class SplashActivity extends AppCompatActivity {

    Button SignIn, SignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (D.sAccountStatus == D.AccountStatus.LOGGED_IN) {
            if (D.sSelf.UserTypeText.equals("collector")) {
                startActivity(new Intent(SplashActivity.this, Main2Activity.class));

            } else {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            }
        }
        SignIn = findViewById(R.id.button2);
        SignIn.setOnClickListener(view -> startActivity(new Intent(SplashActivity.this, SignInActivity.class)));

        SignUp = findViewById(R.id.button);
        SignUp.setOnClickListener(view -> startActivity(new Intent(SplashActivity.this, SignupActivity.class)));
    }

}
