package com.xdev.whitespot.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.adapters.CategoryAdapter;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.Category;
import com.xdev.whitespot.webservices.Order;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Step2Fragment extends Fragment {
    TextView clean, press, total;
    RecyclerView men, women, general, common, SelectedItems, SelectedItems2, SelectedItems3;
    Order order;
    CategoryAdapter menCategory, womenCategory, generalCategory;
    ImageView Men, Women, General, Common;
    String need = "press";
    Button next;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_step2, container, false);
        press = rootView.findViewById(R.id.textView10);
        clean = rootView.findViewById(R.id.textView11);
        men = rootView.findViewById(R.id.recyclerView);
        women = rootView.findViewById(R.id.recyclerView2);
        general = rootView.findViewById(R.id.recyclerView3);
        common = rootView.findViewById(R.id.recyclerView4);
        Men = rootView.findViewById(R.id.menbtn);
        Women = rootView.findViewById(R.id.womenbtn);
        General = rootView.findViewById(R.id.generalbtn);
        Common = rootView.findViewById(R.id.commonbtn);
        SelectedItems = rootView.findViewById(R.id.recyclerView5);
        SelectedItems2 = rootView.findViewById(R.id.recyclerView6);
        SelectedItems3 = rootView.findViewById(R.id.recyclerView7);
        total = rootView.findViewById(R.id.textView15);
        next = rootView.findViewById(R.id.button6);
        order = (Order) getArguments().getSerializable("order");
        getItems();

        clean.setOnClickListener(view -> {
            clean.setTextColor(getActivity().getResources().getColor(R.color.yellow));
            press.setTextColor(getActivity().getResources().getColor(R.color.purple));
            need = "clean";

        });

        press.setOnClickListener(view -> {
            clean.setTextColor(getActivity().getResources().getColor(R.color.purple));
            press.setTextColor(getActivity().getResources().getColor(R.color.yellow));
            need = "press";

        });

        Men.setOnClickListener(view -> men.setVisibility(View.VISIBLE));
        Women.setOnClickListener(view -> women.setVisibility(View.VISIBLE));
        General.setOnClickListener(view -> general.setVisibility(View.VISIBLE));
        Common.setOnClickListener(view -> common.setVisibility(View.VISIBLE));

        next.setOnClickListener(view -> {
            if (D.orderedItems.size() == 0) {
                Toast.makeText(getActivity(), "Please select items", Toast.LENGTH_SHORT).show();
                return;
            }
            Fragment f = new Step3Fragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("order", order);
            bundle.putString("need", total.getText().toString().substring(11));
            f.setArguments(bundle);
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.action_container, f);
            ft.commit();
        });

        return rootView;
    }

    public void getItems() {
        App.getService().getCategories(order.TypeId).enqueue(new Callback<Category>() {
            @Override
            public void onResponse(@NonNull Call<Category> call, @NonNull Response<Category> r) {
                if (r.body() == null || r.body().status != 200) {
                    onFailure(call, new Throwable());
                    return;
                }
                for (Category.AllCategories category : r.body().ItemsOfAllCategories) {
                    switch (category.CategoryEngName) {
                        case "Men":
                            menCategory = new CategoryAdapter(getActivity(), category.Items, SelectedItems, total);
                            men.setLayoutManager(new LinearLayoutManager(getContext()));
                            men.setAdapter(menCategory);
                            break;
                        case "ًWomen":
                            womenCategory = new CategoryAdapter(getActivity(), category.Items, SelectedItems2, total);
                            women.setLayoutManager(new LinearLayoutManager(getContext()));
                            women.setAdapter(womenCategory);
                            break;
                        case "ًGeneral":
                            generalCategory = new CategoryAdapter(getActivity(), category.Items, SelectedItems3, total);
                            general.setLayoutManager(new LinearLayoutManager(getContext()));
                            general.setAdapter(generalCategory);
                            break;
                    }
                }


            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
