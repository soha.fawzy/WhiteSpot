package com.xdev.whitespot.views;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.Utils;
import com.xdev.whitespot.webservices.Order;

import java.util.ArrayList;
import java.util.Calendar;

public class Step1Fragment extends Fragment {
    ImageView regular, half_express, express;
    TextView pickupT, deliverT, Address;
    ConstraintLayout pickupTime, deliverTime;
    int type;
    String deliver, deliverText, pickupText;
    Button Next;
    Calendar c;
    Calendar pickupc, deliverc;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_step1, container, false);
        regular = rootView.findViewById(R.id.imageView4);
        half_express = rootView.findViewById(R.id.imageView5);
        express = rootView.findViewById(R.id.imageView6);

        pickupTime = rootView.findViewById(R.id.constraintLayout3);
        deliverTime = rootView.findViewById(R.id.constraintLayout4);
        pickupT = rootView.findViewById(R.id.textView20);
        deliverT = rootView.findViewById(R.id.textView22);
        Address = rootView.findViewById(R.id.address);
        Next = rootView.findViewById(R.id.button5);
        c = Calendar.getInstance();
        deliverc = Calendar.getInstance();
        pickupc = Calendar.getInstance();
        D.orderedItems = new ArrayList<>();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Order Details");
        //Select OnDoor by Default
        deliver = "inHome";
        Address.setText(D.sSelf.Address);

        regular.setOnClickListener(view -> {
            regular.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_regular));
            half_express.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_express_deactive));
            express.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_expressss));
            type = 1;
        });

        half_express.setOnClickListener(view -> {
            regular.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_regular_deactive));
            half_express.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_express));
            express.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_expressss));
            type = 2;
        });

        express.setOnClickListener(view -> {
            Utils.showSnackBar(getActivity(), R.string.not_working);
        });


        pickupTime.setOnClickListener(view -> {
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            final int hour = c.get(Calendar.HOUR_OF_DAY);
            final int minute = c.get(Calendar.MINUTE);
            pickupText = "";
            new DatePickerDialog(
                    getActivity(), R.style.DatePicker, (view1, year1, month1, dayOfMonth) -> {
                int month2 = month1 + 1;
                pickupText = year1 + "-" + month2 + "-" + dayOfMonth;
                new Handler().postDelayed(() -> new TimePickerDialog(getActivity(), R.style.DatePicker, (view2, hourOfDay, minute1) -> {
                    pickupText += " " + hourOfDay + ":" + minute1;
                    pickupT.setText(pickupText);
                    pickupc.set(year1, month1, dayOfMonth, hourOfDay, minute1);
                }, hour, minute, false).show(), 500);
            }, year, month, day).show();
        });

        deliverTime.setOnClickListener(view -> {
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            final int hour = c.get(Calendar.HOUR_OF_DAY);
            final int minute = c.get(Calendar.MINUTE);
            deliverText = "";
            new DatePickerDialog(
                    getActivity(), R.style.DatePicker, (view1, year1, month1, dayOfMonth) -> {
                int month2 = month1 + 1;
                deliverText = year1 + "-" + month2 + "-" + dayOfMonth;
                new Handler().postDelayed(() -> new TimePickerDialog(getActivity(), R.style.DatePicker, (view2, hourOfDay, minute1) -> {
                    deliverText += " " + hourOfDay + ":" + minute1;
                    deliverT.setText(deliverText);
                    deliverc.set(year1, month1, dayOfMonth, hourOfDay, minute1);

                }, hour, minute, false).show(), 500);
            }, year, month, day).show();
        });

        Next.setOnClickListener(view -> {
            if (pickupText == null) {
                pickupT.setError("please Select Pickup time");
            } else if (deliverText == null) {
                deliverT.setError("Please Select Deliver time");
            } else if (c.after(pickupc) || c.after(deliverc)) {
                Toast.makeText(getActivity(), "this dates are old", Toast.LENGTH_SHORT).show();
            } else if (pickupc.after(deliverc)) {
                Toast.makeText(getActivity(), "deliver date is after pickup date", Toast.LENGTH_SHORT).show();

            } else {
                Fragment f = new Step2Fragment();
                Bundle bundle = new Bundle();
                Order order = new Order(1, deliver, pickupText, deliverText, D.sSelf.Address);
                bundle.putSerializable("order", order);
                f.setArguments(bundle);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.action_container, f);
                ft.commit();
            }
        });

        return rootView;
    }
}
