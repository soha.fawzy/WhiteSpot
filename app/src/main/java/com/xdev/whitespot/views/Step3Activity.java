package com.xdev.whitespot.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.webservices.AddOrderRequest;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.Order;
import com.xdev.whitespot.webservices.StatusResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Step3Activity extends AppCompatActivity {
    ImageView hanging, folded, strach;
    EditText comment;
    String need, packageMethod = "Hanging";
    Button submit;
    Order order;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step3);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.order_details));
        toolbar.setNavigationOnClickListener(view -> finish());
        FloatingActionButton floatingActionButton = findViewById(R.id.float_action);
        floatingActionButton.setOnClickListener(view -> {
            String uri = "tel: 9999";
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        });
        hanging = findViewById(R.id.imageView17);
        folded = findViewById(R.id.imageView18);
        strach = findViewById(R.id.imageView19);
        comment = findViewById(R.id.editText7);
        submit = findViewById(R.id.button7);
        if (getIntent().hasExtra("need")) {
            need = getIntent().getStringExtra("need");
        }
        if (getIntent().hasExtra("order")) {
            order = (Order) getIntent().getSerializableExtra("order");
        }
        hanging.setBackgroundColor(this.getResources().getColor(android.R.color.white));
        folded.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
        strach.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));

        hanging.setOnClickListener(view -> {
            hanging.setImageDrawable(getResources().getDrawable(R.drawable.ic_hang_deactive));
            folded.setImageDrawable(getResources().getDrawable(R.drawable.ic_fold_deactive));
            strach.setImageDrawable(getResources().getDrawable(R.drawable.ic_starch));
            hanging.setBackgroundColor(this.getResources().getColor(android.R.color.white));
            folded.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
            strach.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
            packageMethod = "Hanging";

        });

        folded.setOnClickListener(view -> {
            hanging.setImageDrawable(getResources().getDrawable(R.drawable.ic_hanging));
            folded.setImageDrawable(getResources().getDrawable(R.drawable.ic_folded));
            strach.setImageDrawable(getResources().getDrawable(R.drawable.ic_starch));
            hanging.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
            folded.setBackgroundColor(this.getResources().getColor(android.R.color.white));
            strach.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
            packageMethod = "Folded";

        });

        strach.setOnClickListener(view -> {
            hanging.setImageDrawable(getResources().getDrawable(R.drawable.ic_hanging));
            folded.setImageDrawable(getResources().getDrawable(R.drawable.ic_fold_deactive));
            strach.setImageDrawable(getResources().getDrawable(R.drawable.ic_press_active));
            hanging.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
            folded.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
            strach.setBackgroundColor(this.getResources().getColor(android.R.color.white));
            packageMethod = "Strach";
        });

        submit.setOnClickListener(view -> {
            showProgressDialog();
            List<AddOrderRequest.OrderedItem> orderedItems = new ArrayList<>();
            orderedItems.addAll(D.orderedItems);
            App.getService().addOrder(new AddOrderRequest(new AddOrderRequest.UserData(D.sSelf.Id, D.sSelf.Name, order.Address, order.deliveryWay, packageMethod, order.pickupTime, order.deliverTime, D.sSelf.Number, D.price, order.Address, order.Address, comment.getText().toString()), orderedItems)).enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> r) {
                    if (r.body() == null) {
                        onFailure(call, new Throwable());
                        return;
                    }
                    dismissProgressDialog();
                    startActivity(new Intent(Step3Activity.this, MainActivity.class).putExtra("done", "Done"));
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    t.printStackTrace();
                    dismissProgressDialog();

                }
            });
        });
    }

    private void showProgressDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Submitting");
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    private void dismissProgressDialog() {
        if (null != mDialog) {
            mDialog.dismiss();
        }
    }
}
