package com.xdev.whitespot.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.xdev.whitespot.R;
import com.xdev.whitespot.Utils;
import com.xdev.whitespot.adapters.ItemsAdapter;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.ClientOrdersResponse;
import com.xdev.whitespot.webservices.HistoryResponse;
import com.xdev.whitespot.webservices.OrderDetailsResponse;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends AppCompatActivity {
    ClientOrdersResponse.Orders order;
    TextView Collecting, Address, Name, Pickup, Deliver;
    RecyclerView items;
    HistoryResponse.OrderDetails orderDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        if (getIntent().hasExtra("order")) {
            order = (ClientOrdersResponse.Orders) getIntent().getSerializableExtra("order");
        }
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Address = findViewById(R.id.address);
        Name = findViewById(R.id.textView37);
        Pickup = findViewById(R.id.textView38);
        Deliver = findViewById(R.id.textView39);
        Collecting = findViewById(R.id.textView40);
        items = findViewById(R.id.items);
        getOrdersDetails();
    }

    public void getOrdersDetails() {
        App.getService().getOrderDetails(order.OderId).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                if (response.body() == null) {
                    onFailure(call, new Throwable());
                    return;
                }
                Address.setText(response.body().OrderData.Address);
                Name.setText(response.body().OrderData.Username);
                Pickup.setText("Pickup Time: " + response.body().OrderData.PickupTime);
                Deliver.setText("Delivery Time: " + response.body().OrderData.DeliveryTime);
                Collecting.setText(response.body().OrderData.CollectingMethod);
                ItemsAdapter itemsAdapter = new ItemsAdapter(OrderDetailsActivity.this, response.body().OrderDetails);
                items.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                items.setAdapter(itemsAdapter);
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                t.printStackTrace();
                if (t.fillInStackTrace() instanceof UnknownHostException || t.fillInStackTrace() instanceof SocketTimeoutException) {
                    Utils.showSnackBar(OrderDetailsActivity.this, R.string.no_internet);
                }
            }
        });
    }

}

