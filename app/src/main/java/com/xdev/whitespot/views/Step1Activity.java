package com.xdev.whitespot.views;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xdev.whitespot.D;
import com.xdev.whitespot.R;
import com.xdev.whitespot.Utils;
import com.xdev.whitespot.webservices.Order;

import java.util.ArrayList;
import java.util.Calendar;

public class Step1Activity extends AppCompatActivity {
    ImageView regular, half_express, express;
    TextView pickupT, deliverT, Address;
    ConstraintLayout pickupTime, deliverTime;
    int type;
    String deliver, deliverText, pickupText;
    Button Next;
    Calendar c;
    Calendar pickupc, deliverc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step1);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.order_details);
        toolbar.setNavigationOnClickListener(view -> startActivity(new Intent(Step1Activity.this, MainActivity.class)));
        FloatingActionButton floatingActionButton = findViewById(R.id.float_action);
        floatingActionButton.setOnClickListener(view -> {
            String uri = "tel: 9999";
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        });
        regular = findViewById(R.id.imageView4);
        half_express = findViewById(R.id.imageView5);
        express = findViewById(R.id.imageView6);
        pickupTime = findViewById(R.id.constraintLayout3);
        deliverTime = findViewById(R.id.constraintLayout4);
        pickupT = findViewById(R.id.textView20);
        deliverT = findViewById(R.id.textView22);
        Address = findViewById(R.id.address);
        Next = findViewById(R.id.button5);
        c = Calendar.getInstance();
        deliverc = Calendar.getInstance();
        pickupc = Calendar.getInstance();
        D.orderedItems = new ArrayList<>();
        //Select OnDoor by Default
        deliver = "inHome";
        type = 1;
        Address.setText(D.sSelf.Address);

        regular.setOnClickListener(view -> {
            regular.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_regular));
            half_express.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_express_deactive));
            express.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_expressss_deactive));
            type = 1;
        });

        half_express.setOnClickListener(view -> {
            regular.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_regular_deactive));
            half_express.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_express));
            express.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_expressss_deactive));
            type = 2;
        });

        express.setOnClickListener(view -> {
            Utils.showSnackBar(this, R.string.not_working);
            type = 3;
        });


        pickupTime.setOnClickListener(view -> {
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            final int hour = c.get(Calendar.HOUR_OF_DAY);
            final int minute = c.get(Calendar.MINUTE);
            pickupText = "";
            new DatePickerDialog(
                    this, R.style.DatePicker, (view1, year1, month1, dayOfMonth) -> {
                int month2 = month1 + 1;
                pickupText = year1 + "-" + month2 + "-" + dayOfMonth;
                new Handler().postDelayed(() -> new TimePickerDialog(this, R.style.DatePicker, (view2, hourOfDay, minute1) -> {
                    pickupText += " " + hourOfDay + ":" + minute1;
                    pickupT.setText(pickupText);
                    pickupc.set(year1, month1, dayOfMonth, hourOfDay, minute1);
                }, hour, minute, false).show(), 500);
            }, year, month, day).show();
        });

        deliverTime.setOnClickListener(view -> {
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            final int hour = c.get(Calendar.HOUR_OF_DAY);
            final int minute = c.get(Calendar.MINUTE);
            deliverText = "";
            new DatePickerDialog(
                    this, R.style.DatePicker, (view1, year1, month1, dayOfMonth) -> {
                int month2 = month1 + 1;
                deliverText = year1 + "-" + month2 + "-" + dayOfMonth;
                new Handler().postDelayed(() -> new TimePickerDialog(this, R.style.DatePicker, (view2, hourOfDay, minute1) -> {
                    deliverText += " " + hourOfDay + ":" + minute1;
                    deliverT.setText(deliverText);
                    deliverc.set(year1, month1, dayOfMonth, hourOfDay, minute1);

                }, hour, minute, false).show(), 500);
            }, year, month, day).show();
        });

        Next.setOnClickListener(view -> {
            if (pickupText == null) {
                pickupT.setError("please Select Pickup time");
            } else if (deliverText == null) {
                deliverT.setError("Please Select Deliver time");
            } else if (c.after(pickupc) || c.after(deliverc)) {
                Toast.makeText(this, "this dates are old", Toast.LENGTH_SHORT).show();
            } else if (pickupc.after(deliverc)) {
                Toast.makeText(this, "deliver date is after pickup date", Toast.LENGTH_SHORT).show();

            } else {
                Order order = new Order(type, deliver, pickupText, deliverText, D.sSelf.Address);
                startActivity(new Intent(Step1Activity.this, Step2Activity.class).putExtra("order", order).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));

            }
        });

    }
}
