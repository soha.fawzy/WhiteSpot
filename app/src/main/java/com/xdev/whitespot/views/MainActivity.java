package com.xdev.whitespot.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.xdev.whitespot.R;
import com.xdev.whitespot.UserUtils;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    private FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        FloatingActionButton floatingActionButton = findViewById(R.id.float_action);
        floatingActionButton.setOnClickListener(view -> {
            String uri = "tel: 9999";
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        });
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (getIntent().hasExtra("done")) {
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.action_container, new HistoryFragment());
            ft.commit();
            toolbar.setTitle(R.string.my_orders);

        } else {
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.action_container, new AddOrderFragment());
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.action_container, new AddOrderFragment());
            ft.commit();
            toolbar.setTitle(R.string.home);
        } else if (id == R.id.orders) {
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.action_container, new HistoryFragment());
            ft.commit();
            toolbar.setTitle(R.string.my_orders);
        } else if (id == R.id.setting) {
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.action_container, new ProfileFragment());
            ft.commit();
            toolbar.setTitle(R.string.settings);

        } else if (id == R.id.logout) {
            UserUtils.clearUser(getApplicationContext());
            startActivity(new Intent(MainActivity.this, SignInActivity.class));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
