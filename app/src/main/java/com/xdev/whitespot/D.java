package com.xdev.whitespot;


import com.xdev.whitespot.webservices.AddOrderRequest;

import java.util.ArrayList;
import java.util.List;

public class D {
    public static final String SELF = "com.xdev.whitespot.SELF";
    public static final String ACCOUNT_STATUS = "com.xdev.whitespot.ACCOUNT_STATUS";
    public static User sSelf;
    public static AccountStatus sAccountStatus = AccountStatus.LOGGED_OUT;
    public static float price = 0;
    public static List<AddOrderRequest.OrderedItem> orderedItems = new ArrayList<>();

    private D() {
    }

    public enum AccountStatus {
        LOGGED_OUT,
        LOGGED_IN,
    }
}
