package com.xdev.whitespot.webservices;


import java.io.Serializable;
import java.util.List;

public class ClientOrdersResponse implements Serializable {
    public int Staus;
    public List<Orders> Orders;

    public class Orders implements Serializable {
        public int OderId;
        public String State;
        public float Total;
    }
}
