package com.xdev.whitespot.webservices;

import java.util.List;

public class Category {
    public int status;
    public List<AllCategories> ItemsOfAllCategories;

    public class AllCategories {
        public String CategoryArName, CategoryEngName;
        public List<Item> Items;

        public class Item {
            public int Id;
            public String ArName, EngName, Price;
            public boolean selected;
            public int count;

        }
    }
}
