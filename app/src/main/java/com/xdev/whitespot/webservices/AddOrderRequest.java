package com.xdev.whitespot.webservices;

import java.util.List;

public class AddOrderRequest {
    public UserData UserData;
    public List<OrderedItem> OrderItems;

    public AddOrderRequest(AddOrderRequest.UserData userData, List<OrderedItem> orderItems) {
        UserData = userData;
        OrderItems = orderItems;
    }

    public static class UserData {
        public int UserId;
        public String Username, Address,
                CollectingMethod,
                PackingMethod,
                PickupTime,
                DeliveryTime, Number,
                Area, City, Notes;
        public float TotalPrice;

        public UserData(int userId, String username, String address, String collectingMethod, String packingMethod, String pickupTime, String deliveryTime, String number, float totalPrice, String area, String city, String notes) {
            UserId = userId;
            Username = username;
            Address = address;
            CollectingMethod = collectingMethod;
            PackingMethod = packingMethod;
            PickupTime = pickupTime;
            DeliveryTime = deliveryTime;
            Number = number;
            TotalPrice = totalPrice;
            Area = area;
            City = city;
            Notes = notes;
        }
    }

    public static class OrderedItem {
        public int ItemId, ItemQte;

        public OrderedItem(int itemId, int itemQte) {
            ItemId = itemId;
            ItemQte = itemQte;
        }
    }
}
