package com.xdev.whitespot.webservices;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AppServices {
    @POST("UsersAccountsApi/Login")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @POST("UsersAccountsApi/Signup")
    Call<RegisterResponse> register(@Body RegisterRequest registerRequest);

    @GET("ItemsApi/ItemsOfCategories/")
    Call<Category> getCategories(@Query("Id") int id);

    @POST("OrdersApi/SubmitOrder")
    Call<StatusResponse> addOrder(@Body AddOrderRequest addOrderRequest);

    @POST("AddressesApi/AddNewAddress")
    Call<StatusResponse> addAdress(@Body AddAddressRequest addAddressRequest);

    @GET("OrdersApi/OrdersHistory/")
    Call<HistoryResponse> getHistory(@Query("Id") int id);

    @GET("CollectorsApi/CollectorOrderDetails/")
    Call<OrderDetailsResponse> getOrderDetails(@Query("Id") int id);

    @POST("UsersAccountsApi/EditProfile/")
    Call<LoginResponse> edit(@Body EditProfileRequest editProfileRequest);

    @GET("CollectorsApi/GetCollectorOrders")
    Call<ClientOrdersResponse> getClientOrders(@Query("Id") int id);

    @POST("UsersAccountsApi/ForgetPassword")
    Call<StatusResponse> sendEmail(@Body SendEmailRequest sendEmailRequest);

    @POST("UsersAccountsApi/ConfirmPin ")
    Call<StatusResponse> confirmPin(@Body ConfirmPinRequest confirmPinRequest);

    @POST("UsersAccountsApi/NewPassword")
    Call<StatusResponse> changePass(@Body ChangePasswordRequest changePasswordRequest);
}
