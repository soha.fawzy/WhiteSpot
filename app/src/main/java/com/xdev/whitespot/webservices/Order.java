package com.xdev.whitespot.webservices;

import java.io.Serializable;

public class Order implements Serializable {
    public int TypeId;
    public String deliveryWay, pickupTime, deliverTime, Address;

    public Order(int typeId, String deliveryWay, String pickupTime, String deliverTime, String address) {
        TypeId = typeId;
        this.deliveryWay = deliveryWay;
        this.pickupTime = pickupTime;
        this.deliverTime = deliverTime;
        Address = address;
    }
}
