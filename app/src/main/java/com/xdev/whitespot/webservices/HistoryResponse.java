package com.xdev.whitespot.webservices;


import java.util.List;

public class HistoryResponse {
    public int Status;
    public List<OrderDetails> History;

    public class OrderDetails {
        public int Id;
        public String Date, EngState, ArState, Total, PickupTime, DeliveryTime, CollectingMethod;
        public List<OrderDetailsResponse.OrderDetails> items;


    }
}
