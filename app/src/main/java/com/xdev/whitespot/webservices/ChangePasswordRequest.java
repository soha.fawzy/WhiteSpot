package com.xdev.whitespot.webservices;

/**
 * Created by engsoha on 12/1/17.
 */

public class ChangePasswordRequest {
    String Email, Pin, NewPassword;

    public ChangePasswordRequest(String email, String pin, String newPassword) {
        Email = email;
        Pin = pin;
        NewPassword = newPassword;
    }
}
