package com.xdev.whitespot.webservices;

public class LoginRequest {
    public String Email, Username, Password;

    public LoginRequest(String email, String username, String password) {
        Email = email;
        Username = username;
        Password = password;
    }
}
