package com.xdev.whitespot.webservices;

public class RegisterRequest {
    private String Username, Password, Email, Number, Address;

    public RegisterRequest(String username, String password, String email, String number, String address) {
        Username = username;
        Password = password;
        Email = email;
        Number = number;
        Address = address;
    }
}

