package com.xdev.whitespot.webservices;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App {
    private static final String BASE_URL = "http://wth.search-khana.com/api/";
    private static App mInstance = new App();
    private final AppServices mService;

    private App() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mService = retrofit.create(AppServices.class);
    }

    public static AppServices getService() {
        if (null == mInstance) {
            mInstance = new App();
        }
        return mInstance.getServicee();
    }

    private AppServices getServicee() {
        return mService;
    }

}
