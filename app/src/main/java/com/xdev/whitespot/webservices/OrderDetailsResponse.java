package com.xdev.whitespot.webservices;


import java.util.List;

public class OrderDetailsResponse {
    public OrderData OrderData;
    public List<OrderDetails> OrderDetails;

    public class OrderData {
        public String Username, Address, City, Area, Number, PickupTime, DeliveryTime, CollectingMethod;
        public float Total;
    }

    public class OrderDetails {
        public String ItemArName, ItemEngName;
        public float Price;
        public int Qte;
    }
}
