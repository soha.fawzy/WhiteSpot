package com.xdev.whitespot.webservices;

/**
 * Created by engsoha on 11/27/17.
 */

public class SendEmailRequest {
    String email;

    public SendEmailRequest(String email) {
        this.email = email;
    }
}
