package com.xdev.whitespot.webservices;

public class EditProfileRequest {
    public int Id;
    public String Password, Email, Phone, Address;

    public EditProfileRequest(int id, String password, String email, String phone, String address) {
        Id = id;
        Password = password;
        Email = email;
        Phone = phone;
        Address = address;
    }

    public EditProfileRequest(int id, String email, String phone, String address) {
        Id = id;
        Email = email;
        Phone = phone;
        Address = address;
    }
}
