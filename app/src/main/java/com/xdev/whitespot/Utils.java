package com.xdev.whitespot;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /*
    * Purpose: check if edit text is empty or not
    * and show error messege if it's empty
    * @param editTexts editText or multi editTexts which is want to check
    * @return boolean is empty or not
     */
    public static boolean isEditTextsEmpty(Context context, EditText... editTexts) {
        for (EditText editText : editTexts) {
            if (editText.getText().toString().isEmpty()) {
                editText.requestFocus();
                editText.setError(context.getString(R.string.required_field));
                return true;
            }
        }
        return false;
    }

    public static void clear(EditText... editTexts) {
        for (EditText editText : editTexts) {
            editText.setText("");
        }
    }

    public static boolean isValidUser(EditText editText) {
        String passPattren = "^[a-zA-Z0-9._-]{3,15}";
        Pattern pattern = Pattern.compile(passPattren);
        Matcher matcher = pattern.matcher(editText.getText());
        if (matcher.matches()) {
            return true;
        }
        editText.requestFocus();
        editText.setError("اسم المستخدم يجب أن يتكون من 3 ل 15 حرف وأن لا يحتوي على مسافات");
        return false;
    }

    public static boolean isEmptySpinner(Spinner spinner, String spinnerName) {
        if (spinner.getSelectedItem().equals(spinnerName) || spinner.getSelectedItem().equals("")) {
            TextView errorText = (TextView) spinner.getSelectedView();
            errorText.setError("أختر " + spinnerName);
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            return true;
        }
        return false;
    }


    public static boolean isChecked(Context context, CheckBox checkBox) {
        if (!checkBox.isChecked()) {
            checkBox.requestFocus();
            checkBox.setError(context.getString(R.string.accept_terms));
            return false;
        }
        return true;
    }

    public static boolean isCarbonCopy(Context context, EditText editText1, EditText editText2) {
        if (!editText2.getText().toString().equals(editText1.getText().toString())) {
            editText2.requestFocus();
            editText2.setError(context.getString(R.string.password_not_mached));
            return false;
        }
        return true;
    }

    public static boolean isValidPassword(Context context, EditText password) {
        String passwordPattern = "((?=.*[A-Za-z]).(?=.*[A-Za-z0-9@#$%_])(?=\\S+$).{3,})";
        Pattern pattern = Pattern.compile(passwordPattern);
        Matcher matcher = pattern.matcher(password.getText().toString());
        if (matcher.matches()) {
            return true;
        }
        password.requestFocus();
        password.setError(context.getString(R.string.invalid_password));
        return false;
    }

    public static boolean isValidEmail(Context context, EditText email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email.getText().toString().trim();

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        }
        email.requestFocus();
        email.setError(context.getString(R.string.invalid_email));
        return false;
    }

    public static void showSnackBar(Activity activity, @StringRes int id, @ColorRes int colorId) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), id, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(activity.getResources().getColor(colorId));
        snackbar.show();
    }

    public static void showSnackBar(Activity activity, @StringRes int id) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), id, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public static void clearEditText(EditText... editTexts) {
        for (EditText editText : editTexts) {
            editText.setText("");
        }
    }

    public static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return Character.DIRECTIONALITY_RIGHT_TO_LEFT == directionality || Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC == directionality;
    }

    /* Read and write objects. */
    static void writeObject(Context context, Serializable object, String filename) {
        ObjectOutputStream objectOut = null;
        try {
            FileOutputStream fileOut = context.openFileOutput(filename, Activity.MODE_PRIVATE);
            objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(object);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != objectOut) {
                try {
                    objectOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static <T> T readObject(Context context, String filename) {
        ObjectInputStream objectIn = null;
        Object object = null;
        try {
            FileInputStream fileIn = context.getApplicationContext().openFileInput(filename);
            objectIn = new ObjectInputStream(fileIn);
            object = objectIn.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (null != objectIn) {
                try {
                    objectIn.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            return (T) object;
        } catch (ClassCastException e) {
            e.printStackTrace();
            return null;
        }
    }


}
