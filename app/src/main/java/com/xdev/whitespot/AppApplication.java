package com.xdev.whitespot;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;


public class AppApplication extends Application {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        loadDData();
//        Fonty
//                .context(this)
//                .regularTypeface("Cairo-Regular.ttf")
//                .boldTypeface("Cairo-Bold.ttf")
//                .done();
    }

    /**
     * Load the user object from disk.
     */
    private void loadDData() {
        D.sSelf = Utils.readObject(this, UserUtils.USER_FILE);
        if (null == D.sSelf) {
            D.sSelf = new User();
            D.sAccountStatus = D.AccountStatus.LOGGED_OUT;
            return;
        }

        D.AccountStatus status = Utils.readObject(this, UserUtils.ACCOUNT_STATUS);
        if (null != status) {
            D.sAccountStatus = status;
        }

    }

}
