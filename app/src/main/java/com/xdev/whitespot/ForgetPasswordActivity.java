package com.xdev.whitespot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.xdev.whitespot.views.SignInActivity;
import com.xdev.whitespot.webservices.App;
import com.xdev.whitespot.webservices.ChangePasswordRequest;
import com.xdev.whitespot.webservices.ConfirmPinRequest;
import com.xdev.whitespot.webservices.SendEmailRequest;
import com.xdev.whitespot.webservices.StatusResponse;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {

    EditText email, code, password, confirmPassword;
    Button send;
    String pin;
    int stage = 0;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        email = findViewById(R.id.editText);
        code = findViewById(R.id.editText10);
        password = findViewById(R.id.editText3);
        confirmPassword = findViewById(R.id.editText11);
        send = findViewById(R.id.button3);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (stage) {
                    case 0:
                        if (!Utils.isEditTextsEmpty(ForgetPasswordActivity.this, email)) {
                            showProgressDialog();
                            App.getService().sendEmail(new SendEmailRequest(email.getText().toString())).enqueue(new Callback<StatusResponse>() {
                                @Override
                                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                                    if (response.body() == null) {
                                        onFailure(call, new Throwable());
                                        return;
                                    }
                                    if (response.body().Status == 200) {
                                        stage = 1;
                                        D.sSelf.Email = email.getText().toString();
                                        UserUtils.saveUser(getApplicationContext());
                                        email.setVisibility(View.GONE);
                                        code.setVisibility(View.VISIBLE);
                                    } else {
                                        email.setError(getString(R.string.invalid_email));
                                    }
                                    dismissProgressDialog();
                                }

                                @Override
                                public void onFailure(Call<StatusResponse> call, Throwable t) {
                                    t.printStackTrace();
                                    dismissProgressDialog();
                                    if (t.fillInStackTrace() instanceof UnknownHostException) {
                                        Utils.showSnackBar(ForgetPasswordActivity.this, R.string.no_internet);
                                    }

                                }
                            });
                        }
                        break;
                    case 1:
                        if (!Utils.isEditTextsEmpty(getApplicationContext(), code)) {
                            pin = code.getText().toString();
                            showProgressDialog();
                            App.getService().confirmPin(new ConfirmPinRequest(pin, D.sSelf.Email)).enqueue(new Callback<StatusResponse>() {
                                @Override
                                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                                    if (response.body() == null) {
                                        onFailure(call, new Throwable());
                                        return;
                                    }
                                    if (response.body().Status == 200) {
                                        stage = 2;
                                        code.setVisibility(View.GONE);
                                        password.setVisibility(View.VISIBLE);
                                        confirmPassword.setVisibility(View.VISIBLE);
                                    } else {
                                        code.setError("الكود غير صحيح");
                                    }
                                    dismissProgressDialog();
                                }

                                @Override
                                public void onFailure(Call<StatusResponse> call, Throwable t) {
                                    t.printStackTrace();
                                    dismissProgressDialog();
                                    if (t.fillInStackTrace() instanceof UnknownHostException) {
                                        Utils.showSnackBar(ForgetPasswordActivity.this, R.string.no_internet);
                                    }

                                }
                            });
                        }
                        break;
                    case 2:
                        if (!Utils.isEditTextsEmpty(getApplicationContext(), password, confirmPassword) && Utils.isValidPassword(getApplicationContext(), password) && Utils.isCarbonCopy(getApplicationContext(), password, confirmPassword)) {
                            showProgressDialog();
                            App.getService().changePass(new ChangePasswordRequest(D.sSelf.Email, pin, password.getText().toString())).enqueue(new Callback<StatusResponse>() {
                                @Override
                                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                                    if (response.body() == null && response.body().Status != 200) {
                                        onFailure(call, new Throwable());
                                        return;
                                    }
                                    Utils.showSnackBar(ForgetPasswordActivity.this, R.string.done);
                                    startActivity(new Intent(ForgetPasswordActivity.this, SignInActivity.class));

                                }

                                @Override
                                public void onFailure(Call<StatusResponse> call, Throwable t) {
                                    t.printStackTrace();
                                    dismissProgressDialog();
                                    if (t.fillInStackTrace() instanceof UnknownHostException || t.fillInStackTrace() instanceof SocketTimeoutException) {
                                        Utils.showSnackBar(ForgetPasswordActivity.this, R.string.no_internet);
                                    }
                                }
                            });
                        }
                        break;
                }
            }
        });
    }

    private void showProgressDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.loading));
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    private void dismissProgressDialog() {
        if (null != mDialog) {
            mDialog.dismiss();
        }
    }
}
